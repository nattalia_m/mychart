<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('/web/js/Chart.bundle.min.js', ['position' => \yii\web\View::POS_HEAD]);
$this->registerJsFile('/web/js/moment.min.js', ['position' => \yii\web\View::POS_HEAD]);
?>
<h3>График баланса для счета</h3>
<div class="row">
    <div class="col-md-8">
        <? $form = ActiveForm::begin(['options' => [
            'class' => 'form-inline',
            'enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($model, 'dataFile')->fileInput()->label('Выберите файл'); ?>
        <?= Html::submitButton('Построить график', ['class' => 'btn btn-primary']); ?>
        <?php ActiveForm::end() ?>
    </div>
    <div class="col-md-4">
        <? if ($model->labelBuy): ?>
            <table class="table table-bordered">
                <tr>
                    <th>Обработан файл:</th>
                    <td><?= $model->dataFile->baseName . '.' . $model->dataFile->extension; ?></td>
                </tr>
                <tr>
                    <th>Account:</th>
                    <td><?= $model->account; ?></td>
                </tr>
                <tr class="warning">
                    <th>Баланс:</th>
                    <td><?= $model->balance . ' ' . $model->curr; ?></td>
                </tr>
            </table>
        <? endif; ?>
    </div>
</div>

<? if (!$model->errors): ?>
    <? if ($model->labelBuy): ?>
        <div class="row">
            <canvas id="myChart"></canvas>
        </div>
        <script>
            var config = {
                    type: 'line',
                    data: {
                        labels: [<?=$model->labelBuy ?>],
                        datasets: [{
                            label: "Balance",
                            data: [<?=$model->dataBuy ?>],
                            lineTension: 0.1,
                            backgroundColor: "rgba(75,192,192,0.4)",
                            borderColor: "rgba(75,192,192,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "rgba(75,192,192,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(75,192,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                type: 'time',
                                time: {
                                    tooltipFormat: 'll HH:mm',
                                    displayFormats: {
                                        'millisecond': 'MMM YYYY',
                                        'second': 'MMM YYYY',
                                        'minute': 'MMM YYYY',
                                        'hour': 'MMM YYYY',
                                        'day': 'MMM YYYY',
                                        'week': 'MMM YYYY',
                                        'month': 'MMM YYYY',
                                        'quarter': 'MMM YYYY',
                                        'year': 'MMM YYYY',
                                    }
                                },
                                scaleLabel: {
                                    beginAtZero: false
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                        ,
                    }
                }
                ;

            var ctx = document.getElementById("myChart").getContext("2d");
            new Chart(ctx, config);

        </script>
    <? endif; ?>
<? else: ?>
    <div class="alert alert-danger" role="alert">
        <?= $err = implode(',', $model->errors['table']); ?>
    </div>
<? endif; ?>





