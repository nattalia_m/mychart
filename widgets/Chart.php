<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Account;
use yii\web\UploadedFile;

class Chart extends Widget
{    /**
     * @return string
     */
    public function run()
    {

        $model = new Account();

        if (Yii::$app->request->isPost) {
            $model->dataFile = UploadedFile::getInstance($model, 'dataFile');
            if ($model->upload()) {
                // file is uploaded successfully
                $model->getDataBuy();
                return $this->render('chart', ['model' => $model]);
            }
        }

        return $this->render('chart', ['model' => $model]);
    }


}