<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $dataFile;
    public $dataBuy;
    public $labelBuy;
    public $listAccounts;

    public function rules()
    {
        return [
            [['dataFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html', 'maxSize'=>'1000000'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->dataFile->saveAs('uploads/' . $this->dataFile->baseName . '.' . $this->dataFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function getDataBuy()
    {
        $file = file_get_contents('uploads/' . $this->dataFile->baseName . '.' . $this->dataFile->extension);
        $doc = \phpQuery::newDocument($file);

        // проверка заголовока таблицы
        $head_abs = 'TicketOpenTimeTypeSizeItemPriceS/LT/PCloseTimePriceCommissionTaxesSwapProfit';
        $head = '';
        foreach ($doc->find('table tr:eq(2)') as $hd) {
            $hd = pq($hd);
            $head = $head . $hd->text();
        }
        $head = str_replace(array("\r\n", "\r", "\n", " "), '', $head);

        if ($head != $head_abs) {
            $this->addError('table', 'Файл неверной структуры!');
            return false;
        }

        $balance = 0;
        foreach ($doc->find('table tr') as $row) {
            $row = pq($row);
            $action = $row->find('td:eq(2)')->text();
            if (trim($action) == 'buy') {
                $buy = $row->find('td:eq(13)')->text();
                $balance = $balance + (int)$buy;
                $r[] = $balance;
                $dt = str_replace(".", "-", $row->find('td:eq(1)')->text());
                $l[] = 'moment("' . $dt . '")';
            }
        }

        //сформировать список значений для графика
        $this->dataBuy = implode(",", $r);
        $this->labelBuy = implode(",", $l);
        return true;
    }


}