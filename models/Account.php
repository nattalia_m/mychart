<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class Account extends Model
{
    /**
     * @var UploadedFile
     */
    public $dataFile;
    public $dataBuy;
    public $labelBuy;
    public $account;
    public $balance;
    public $curr;

    public function rules()
    {
        return [
            [['dataFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html', 'maxSize' => '1000000'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->dataFile->saveAs(\Yii::$app->params['uploads'] . $this->dataFile->baseName . '.' . $this->dataFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function getDataBuy()
    {
        $file = file_get_contents(\Yii::$app->params['uploads'] . $this->dataFile->baseName . '.' . $this->dataFile->extension);
        $doc = \phpQuery::newDocument($file);

        // проверка заголовока таблицы
        $head_abs = 'TicketOpenTimeTypeSizeItemPriceS/LT/PCloseTimePriceCommissionTaxesSwapProfit';
        $head = '';
        foreach ($doc->find('table tr:eq(2)') as $hd) {
            $hd = pq($hd);
            $head = $head . $hd->text();
        }
        $head = str_replace(array("\r\n", "\r", "\n", " "), '', $head);

        if ($head != $head_abs) {
            $this->addError('table', 'Файл неверной структуры! Выберите другой файл.');
            return false;
        }
        $this->account = substr($doc->find('table tr:eq(0) td:eq(0)')->text(), -7);
        $this->curr = substr($doc->find('table tr:eq(0) td:eq(2)')->text(), -3);

        $balance = 0;
        foreach ($doc->find('table tr') as $row) {
            $buy = 0;
            $row = pq($row);
            $isOper = intval($row->find('td:eq(0)')->text());
            if ($isOper) {
                $action = $row->find('td:eq(2)')->text();
                if (trim($action) == 'balance') {
                    $buy = floatval(str_replace(" ", "", $row->find('td:eq(4)')->text()));
                } else {
                    $commission = floatval(str_replace(" ", "", $row->find('td:eq(10)')->text()));
                    $taxes = floatval(str_replace(" ", "", $row->find('td:eq(11)')->text()));
                    $swap = floatval(str_replace(" ", "", $row->find('td:eq(12)')->text()));
                    $profit = floatval(str_replace(" ", "", $row->find('td:eq(13)')->text()));
                    $buy = $profit + $swap + $taxes + $commission;
                }
                if ($buy != 0) {
                    $balance = $balance + $buy;
                    $r[] = $balance;
                    $dt = str_replace(".", "-", $row->find('td:eq(1)')->text());
                    $l[] = 'moment("' . $dt . '")';
                }
            }

            if (trim($row->find('td:eq(0)')->text()) == 'Balance:') {
                $this->balance = $row->find('td:eq(1)')->text();
            }
        }

        if ($r) {
            $this->dataBuy = implode(",", $r);
            $this->labelBuy = implode(",", $l);
        } else {
            $this->addError('table', 'Нет значений для построения графика.');
            return false;
        }
        return true;
    }


}